milkman.js Copyright (c) 2022 Jill Nesbit

milkman.js is licensed under V3 of the GNU General Public License, the terms of which can be found at https://www.gnu.org/licenses/gpl-3.0.html

However, to allow for more free usage of the library, there is a special exception:

You are expressly permitted by the author of milkman.js to use it as part of non-GPL projects without distributing said projects under GPL, so long as certain conditions are met:
- The project must be strictly non-commercial
- You must include a copy of this license when distributing your project
- If you modify the library code, you must disclose the source of your modified version and distribute it under the same license

You MAY modify the file "boyd.json" without it being classified as modifying the library code.

Should these conditions not be met, you must follow the terms of the GNU GPLv3 as outline above.

Based on code originally created by Double Fine Productions.