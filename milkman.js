class BoydRant {
    #_lastRant = null;
    #iSingular = 0;

    #lastSaid = {};

    constructor(rantData, mutterChance = 0.5, victimChance = 0.5, transitionalChance = 0.5, allowInfinite = false)
    {
        this.rantData = rantData;
        this.mutterChance = mutterChance;
        this.victimChance = victimChance;
        this.transitionalChance = transitionalChance;

        this.allowInfinite = allowInfinite;
    }

    #random( max = 1 )
    {
        return Math.random() * max;
    }
    #randomRange( min, max )
    {
        return Math.random() * ( max - min ) + min;
    }

    #RandInt(iSeedLow = 0, iSeedHigh = 0)
    {
        if ( iSeedLow != 0 && iSeedHigh != 0 )
        {
            return Math.floor( this.#random( iSeedLow, iSeedHigh ) );
        }
        else if ( iSeedLow != 0 )
        {
            return Math.floor( this.#random( iSeedLow ) );
        }
        else
        {
            return Math.floor( this.#random( 1.9 ) );
        }
    }

    CoinFlip()
    {
        return this.#RandInt() > 0;
    }

    #randLinecode(type)
    {
        var arr = this.rantData[type];
        var rand = this.#random();

        var selected = arr[Math.floor(rand*arr.length)];

        while ( this.#lastSaid[type] != null && this.#lastSaid[type].voice == selected.voice )
        {
            var rand = this.#random();
            selected = arr[Math.floor(rand*arr.length)];
            console.log(`${type} last said ${this.#lastSaid[type].quote}, next attempt ${selected.quote}`)
        }

        this.#lastSaid[type] = selected;

        selected.pieceType = type;
        return selected;
    }

    #AddToRant(type, lineDelay = 0)
    {
        var linecode = this.#randLinecode(type);
        linecode.lineDelay = lineDelay;
        this.#_lastRant.push(linecode);
    }

    #Mutter( bAdvanced )
    {
        if ( this.#random() <= this.mutterChance )
        {
            var bAdv = this.CoinFlip();

            if ( !bAdvanced || !bAdv )
                this.#AddToRant( "mutters", 0.5 );
            else
                this.#AddToRant( "advancedMutters", 0.5 );
        }
    }

    #LoopingAction()
    {
        this.#AddToRant("loopingActions");
        this.#Mutter(false);
        this.#RantStart();
    }

    #TermOrSemi()
    {
        this.#iSingular = 0;

        this.#Mutter(false);

        if ( this.CoinFlip() )
        {
            this.#Terminal();
        }
        else
        {
            this.#Semi();
        }
    }

    #Terminal()
    {
        this.#AddToRant("terminals");
        this.#Mutter(true);
        this.#Linkback();
    }

    #Semi()
    {
        this.#AddToRant("semiTerminals");
        this.#AddToRant("conspirators");

        this.#Mutter(false);

        this.#Victim();
    }

    #Linkback()
    {
        if ( this.CoinFlip() )
        {
            this.#AddToRant("linkbackLines");
            this.#Mutter(false);
            this.#TermOrSemi();
        }
        else
        {
            this.#Mutter(false);
            this.#Transitional();
        }
    }

    #Victim()
    {
        if ( this.#random() <= this.victimChance )
        {
            this.#AddToRant("victimLines");
            this.#AddToRant("victims");
        }

        this.#Mutter(false);

        this.#Linkback();
    }

    #Transitional()
    {
        if ( this.#random <= this.transitionalChance )
        {
            this.#AddToRant("transitionals");
        }

        this.#AddToRant('nonsequitors', 2);

        this.#Mutter(true);

        if ( !this.allowInfinite )
            return;

        if ( this.CoinFlip() )
        {
            this.#RantStart(2);
        }

        //End of rant
    }
    

    #RantStart(initialDelay = 0)
    {
        if ( this.CoinFlip() )
        {
            this.#AddToRant( "singularConspirators",  initialDelay);
            this.#iSingular++;
        }
        else
        {
            this.#AddToRant( "conspirators", initialDelay );
            this.#iSingular+=2;
        }

        this.#Mutter( false );

        if ( this.CoinFlip() || this.#iSingular == 1 )
        {
            this.#LoopingAction();
        }
        else
        {
            this.#TermOrSemi();
        }
    }

    #BuildNewRant()
    {
        /*
        if ( !this.allowInfinite && this.CoinFlip() )
        {
            this.#AddToRant("nonsequitors");
            return;
        }
        */
        this.#RantStart()
    }

    newRant()
    {
        this.#_lastRant = new Array();
        this.#BuildNewRant();

        return this.#_lastRant;
    }
}