## milkman.js

This library is a customisable, mostly accurate reimplmentation of the algorithm used to construct Boyd Cooper's insane ranting in the very excellent game Psychonauts.
It is based on code originally written by Anna Kipnis for the game.

The code isn't pretty, though this project was mainly for fun anyway.

See also the C# library - [Milkman](https://gitlab.com/scrunguscrungus/milkman/) - upon which the architecture of this library is heavily based.

### Usage
[Demo](https://jillcrungus.com/projects/psychonauts/boydrant/)

I have no idea what the use case for this library would really be, but regardless:
- Include milkman.js or milkman.min.js on your page
`<script src="milkman.min.js"></script>`
- Create a new `BoydRant` instance and pass your rant data object in the first parameter. For additional configuration options, see `milkman.js`
`boyd = new BoydRant(data);`
- Retrieve a rant using `boyd.newRant()`. This returns an array of rant pieces. Each rant piece is an object containing the `quote` (the actual line), `voice` (the audio file), `pieceType` (the type of rant piece it is) and `lineDelay` (the delay Boyd would have in the game before speaking this piece)
- Process this data however you wish



#### RantData
Before using the library, you need to have RantData JSON. A default file, `boyd.json`, has been provided which shows how these files are structured.
Every array is required, however only the `quote` field is required for each LineCode. `voice` may be provided to tie the linecode to a sound file for audio-based implementations. `og_alt` and `voice_alt` are both deprecated and will have no effect.

Each array's purpose is as follows:
 - **singularConspirators**: singular entities that will be regarded as conspiring against the ranter. e.g. `the kid with the goggles`, `a secret doomsday cult`, `big oil`
 - **conspirators**: Conspirators that can be regarded as a group of entities. e.g. `the squirrels`, `the psychowhatsits`, `the cows`
 - **loopingActions**: Lines that can be chained infinitely to keep bringing more conspirators into the rant. e.g. `and`, `or else, maybe`, `with the backing of`
 - **semiTerminals**: Accusatory lines that can be used to bring an additional conspirator into the rant. e.g. `sold their soul to`, `went to the prom with`, `are working for`
 - **terminals**: Accusatory lines that do NOT bring additional conspirators into the rant. e.g. `won't stop visiting me.`, `have everyone fooled.`, `are not to be trusted.`
 - **linkbackLines**: Lines that can be used to extend the rant by bridging to additional terminals or semi-terminals, i.e. `and they obviously`, `and, let's just say for now that they`, `but they can't hide that they`
 - **victimLines**: Lines that can be used to bring a victim into the rant, used after semi-terminals. e.g. `to keep down`, `to get`, `and who wins? Them. Who loses?`
 - **victims**: Victims that will be used as the subject of the victim lines. i.e. `all of us.`, `my hooch.`, `the water supply.`,
 - **transitionals**: Lines that can be used to finalise the rant. i.e. `How long do they think they can hide that?`, `f they find out I know this stuff, I'm dead`, `...right under people's noses!`
 - **nonsequitors**: Non-sequitors that will occasionally be used instead of a full rant. i.e. `I have to get rid of some of this stuff!`, `But when that happens, they turn it into CHOCOLATE milk, and nobody can tell the difference!`, `They think the windows are tinted, but they ain't tinted nearly enough!`
 - **mutters**: Random muttering that will be interspersed with the elements of the rant. i.e. `...uh...`, `...heh heh...`, `...er...`
 - **advancedMutters**: Similar to mutters, used less frequently and only in specific places. i.e. `...wait...` `...what?...`, `...okay, okay, but...`

 
